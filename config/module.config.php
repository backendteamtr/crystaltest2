<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Crystaltest\Controller\MainController' => 'Crystaltest\Controller\MainController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'crystaltest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crystaltest[/:action][/view/:view][/layout/:layout][/page/:page][/id/:id][/]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Crystaltest\Controller\MainController',
                        'action' => 'admin',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Crystaltest' => __DIR__ . '/../view',
        ),
    ),
);
