<?php
return array(
	'controllers' => array(
		'invokables' => array(
			'Crystaltest\Controller\Crystaltest' => 'Crystaltest\Controller\CrystaltestController'
		)
	),
	'router' => array(
		'routes' => array(
			'crystaltest' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/crystaltest[/:action][/:id][/:email]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+'
					),
					'defaults' => array(
						'controller' => 'Crystaltest\Controller\Crystaltest',
						'action' => 'index'
					)
				)
			)
		)
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'crystaltest' => __DIR__ . '/../view'
		),
	)
);