<?php

namespace Crystaltest\Controller;

use RAD\Lib\Adapter\DataBase;
use RAD\Lib\Entity\RelationEntity;
use RAD\Lib\Entity\FieldsEntity;
use Zend\Session\Container;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Crypt\BlockCipher;
use Zend\Crypt\Symmetric\Mcrypt;
use RAD\Lib\Entity\SchoolsEntity;
use RAD\Lib\Service\Service;
use DOMPDFModule\View\Model\PdfModel;

class MainController extends AbstractActionController {

    public $session;

    public function __construct() {
        //Verify settings
        date_default_timezone_set('America/Los_Angeles');
        $this->session = new Container('crystaltest');
        $this->session->route = 'crystaltest';
        $this->session->toolBar = 'crystaltest/admin/layout/toolbar.phtml';
        $this->session->paginationLayout = 'crystaltest/admin/layout/pager.phtml';
        $this->session->defaultLayout = 'home';
        $this->session->defaultView = 'main';
        $this->session->defaultDB = 'crystaltestdev';
        if ($_SERVER['SERVER_NAME'] == 'apps.ccsd.net' || $_SERVER['SERVER_NAME']=='mobileapps.ccsd.net') 
        $this->session->defaultDB = 'crystaltestprod';
        $this->session->action = 'admin';
       
    }

    public function adminAction() {


        if (!$this->zfcInterActAuthentication()->hasIdentity()) {
            $redirect = $this->url()->fromRoute($this->session->route);

            $this->getRequest()->getQuery()->set('redirect', $redirect);

            return $this->forward()->dispatch('InterActProfile', array(
                        'action' => 'login'
            ));
        }

        $this->session->fcuser = $this->zfcInterActAuthentication()->getIdentity()->getFCUser();
        $this->session->layout = $this->params()->fromRoute('layout', $this->session->defaultLayout);
        $this->session->view = $this->params()->fromRoute('view', $this->session->defaultView);
        $this->session->id = $this->params()->fromRoute('id', 0);


        //This is so we can get the current user info from the application(if they are a controller or some thing else) and store it in the session user. JP
        $connection = new \MongoClient();
        $dbName = $this->session->defaultDB;
        $db = $connection->$dbName;
        $collectionTable = $db->users;
        $this->session->user = (array) $collectionTable->findOne(array('CLIENTID' => $this->session->fcuser["CLIENTID"]));
        //End of add.
        
        $request = $this->getRequest();
        if ($request->isPost()) {

            $this->session->post = array();
            $this->session->post = $request->getPost()->toArray();
        }
        //Verify Route
        $this->session->path = "crystaltest/admin/" . $this->session->view . "/" . $this->session->layout . "/";

        $service = new Service();
        $view = new ViewModel(array(
            'session' => $this->session,
            'service' => $service,
        ));


        $view->setTemplate($this->session->path . $this->session->layout);
        return $view;
    }
    
        public function publicAction() {

        $this->session->defaultView = 'emails';
        $this->session->defaultLayout = 'login';
        $this->session->pageCount = 100;
        $this->session->toolBar = 'crystaltest/public/layout/toolbar.phtml';

       // $this->session->fcuser = $this->zfcInterActAuthentication()->getIdentity()->getFCUser();
        $this->session->layout = $this->params()->fromRoute('layout', $this->session->defaultLayout);
        $this->session->view = $this->params()->fromRoute('view', $this->session->defaultView);
        $this->session->page = $this->params()->fromRoute('page', $this->session->page);
        $this->session->id = $this->params()->fromRoute('id', $this->session->id);

       

        //$this->session->user = $dbUsersFields->unnormalizeData($entity->pid);

        $request = $this->getRequest();
        if ($request->isPost()) {

            $this->session->post = array();
            $this->session->post = $request->getPost()->toArray();
        }
        //Verify Route
        $this->session->path = "crystaltest/public/" . $this->session->view . "/" . $this->session->layout . "/";

        $service = new Service();
        $view = new ViewModel(array(
            'session' => $this->session,
              'service' => $service,
        ));


        $view->setTemplate($this->session->path . $this->session->layout);
        return $view;
    }
    
      public function pdfAction() {
        $service = new Service();
        // manually set total pages of PDF
        // 2013-2014 had 18 total
        // 2014-2015 is 18 also, but HS have an extra page for HS Grad Rates which makes it 19
        $this->session->id = $this->params()->fromRoute('id', 0);
        
        $data = $service->mongoFindOne($this->session->defaultDB, 'main', $this->session->id);
        //echo"<pre>";var_dump($data);exit;
        $pdf = new PdfModel();
        
        $template = 'crystaltest/templates/pdf';
        
        $pdf->setTemplate($template);
        $pdf->setOption('filename', 'crystaltest.'.date('m.d.Y', time())); // prompts a download instead of inline viewing
        $pdf->setOption('paperSize', 'a4');
        $pdf->setVariables(array(
        'data' => $data,
        ));

        return $pdf;
    }
     
    
}



